volScalarField rAU(1.0/UEqn.A());
surfaceScalarField rAUf("rAUf", fvc::interpolate(rAU));
volVectorField HbyA(constrainHbyA(rAU*UEqn.H(), U, p));
volVectorField EbyA("EbyA", -E*ampholytes.rho_e()*rAU);

surfaceScalarField phiHbyA
(
    "phiHbyA",
    fvc::flux(HbyA)
    + fvc::interpolate(rAU)*fvc::ddtCorr(U, phi)
);

MRF.makeRelative(phiHbyA);

adjustPhi(phiHbyA, U, p);

tmp<volScalarField> rAtU(rAU);

surfaceScalarField phiEbyA
(
    "phiEbyA",
    (fvc::interpolate(EbyA) & mesh.Sf())
);

//NO LO PROBARIA EN CONSISTENT!! HAY QUE HACER LAS CUENTAS BIEN!!
/*
if (pimple.consistent())
{
    rAtU = 1.0/max(1.0/rAU - UEqn.H1(), 0.1/rAU);
    phiHbyA +=
      fvc::interpolate(rAtU() - rAU)*(fvc::snGrad(p)*mesh.magSf());
    
    HbyA -= (rAU - rAtU())*fvc::grad(p);
}
*/

if (pimple.nCorrPISO() <= 1)
{
    tUEqn.clear();
    //    tUEqn2.clear();
}

// Update the pressure BCs to ensure flux consistency
constrainPressure(p, U, phiHbyA, rAtU(), MRF);

// Non-orthogonal pressure corrector loop
while (pimple.correctNonOrthogonal())
{
    // Pressure corrector
    fvScalarMatrix pEqn
    (
        fvm::laplacian(rAtU(), p) == fvc::div(phiHbyA) + fvc::div(EbyA)
    );

    pEqn.setReference(pRefCell, pRefValue);

    pEqn.solve(mesh.solver(p.select(pimple.finalInnerIter())));

    if (pimple.finalNonOrthogonalIter())
    {
        phi = phiHbyA + phiEbyA - pEqn.flux();
    }
}

#include "continuityErrs.H"

// Explicitly relax pressure for momentum corrector
p.relax();

U = HbyA + EbyA - rAtU()*fvc::grad(p);
U.correctBoundaryConditions();
//fvOptions.correct(U);
