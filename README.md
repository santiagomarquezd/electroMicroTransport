-------------------------------------------------------------------------------
      electroMicroTransport | Copyright (C) 2016-2024 Santiago Marquez Damian
                            |                         Pablo Alejandro Kler 
                            |                         Gabriel Gerlero
-------------------------------------------------------------------------------

(1) CREDITS

OpenFOAM(R) coding by Santiago Marquez Damian, Pablo A. Kler and Gabriel Gerlero.

Bug reporting and testing by Federico Schaumburg and Gabriel Gerlero.

Docker support by Gabriel Gerlero.

(2) DIRECTORY STRUCTURE

Main solver, ampholyte classes and new boundary conditions

libraries/
 + -> ampholyteMixture
 + -> electroMicroTransportBCs/fields/fvPatchFields/derived/SmoluchowskyWallVelocity
 + -> electroMicroTransportBCs/fields/fvPatchFields/derived/uniformCurrentDensity
 
Tutorials (see details below)

tutorials/
 + -> IEF
 + -> EOF2D
 + -> CZE
 + -> ITP
 + -> paperBasedFFIEF
 + -> paperBasedZE
 + -> paperBasedZE1
 + -> paperBasedZE2
 + -> paperBasedMBE1
 + -> paperBasedMBE2
 
(3) CODE ORGANIZATION

The code is organized in four classes (two for species management and two other
ones for the boundary conditions) plus the code for the solver itself.
The base class is ampholyte which reads the initial concentration distribution
for each ampholyte as long as its physical properties. 

The next level in code organization is the class ampholyteMixture, whose main
objective is to solve a transport equation for each ampholyte and provide group
properties.

The SmoluchowskyWallVelocity boundary condition prescribes a velocity due to the
electroosmotic phenomenon. uniformCurrentDensity prescribes a given current
density along the domain.

Finally the solver, electroMicroTransport, sequentially solves for proton
equilibria, electric field, electrokinetic potential, fluid flow and mass 
transport.

(4) TUTORIAL DESCRIPTIONS

IEF

Solves an isoelectric focusing experiment on a standard capillary. 

EOF2D

Solves a microchip electrophoretic separation involving injection and 
separation steps.

CZE

Solves a capillary zone electrophoretic separation while considering 
electromigrative dispersion.

ITP

Solves a cationic isotachophoretic experiment.

paperBasedFFIEF

Solves a free-flow isoelectric focusing experiment using a paper-based channel.

paperBasedZE, paperBasedZE1, paperBasedZE2

Solve zone electrophoretic separations using a paper-based channel.

paperBasedMBE1, paperBasedMBE2

Solve paper-based moving-boundary electrophoresis experiments.

Additional information on the tutorials can be found in README.md files placed
in their directories.

(5) INSTALLATION AND EXECUTION

Installation from source

Installation of electroMicroTransport from source requires a Linux system and an
installation of OpenFOAM(R) (www.openfoam.com).

After downloading the source code of electroMicroTransport, compile and install
it by running the following commands in a terminal:

$ cd applications/electroMicroTransport

$ ./Allwclean

$ ./Allwmake

To be able to run the bundled tutorials, Python 3.9 or later is required, with 
compatible versions of the NumPy (https://numpy.org) library and electrolytes 
(https://github.com/microfluidica/electrolytes) package installed. These may
be installed with the following commands:

$ python3 -m pip install --upgrade pip

$ python3 -m pip install -r tutorials/requirements.txt

This version of electroMicroTransport is tested with OpenFOAM(R) versions
from 2006 to 2406.

Docker image

As an alternative to compilation from source, a Docker image is available with
the latest pre-compiled version of the electroMicroTransport toolbox. Notably,
this method does not require an existing installation of OpenFOAM(R) and skips
the compilation steps required to install from source. The Docker image also
allows the toolbox to be used on non-Linux systems.

To run electroMicroTransport with this method, follow the instructions for your
platform to install Docker (https://www.docker.com) if necessary. When Docker
is available, the following command downloads and runs electroMicroTransport in
a container:

$ docker run -it microfluidica/electromicrotransport

Refer to the Docker documentation for extra options that allow e.g. running
graphical applications in the container, and the transfer of data between the
Docker container and the host system. Non-Linux users: note that a
case-sensitive filesystem may be necessary to copy simulation cases and results
from the Docker container into the host system without modification (otherwise
you may need to rename some files or archive/compress the case directories
before transferring them).

First steps

Once installed, electroMicroTransport can be used as any other OpenFOAM(R)
application. The easiest way to get started is to copy and run any of the
included tutorial cases. Each tutorial can be executed by running the following
commands inside its case directory:

$ ./inicond.sh

$ electroMicroTransport

(6) TESTING

During development, the toolbox's functionality is checked with an automated
test suite. To run these tests locally, use the following command:

$ tests/Alltest

(7) USER GUIDE

For further details in the previous topics please refer to the User Guide.

(8) REFERENCES

Main references

S. Márquez Damián, F. Schaumburg and P.A. Kler. Open-source toolbox for
electromigrative separations. Computer Physics Communications 237:244-252, 2019.
https://www.sciencedirect.com/science/article/pii/S0010465518304077?via%3Dihub

Gabriel S. Gerlero, Santiago Márquez Damián, Federico Schaumburg,
Nicolás Franck, Pablo A. Kler. Numerical simulations of paper-based
electrophoretic separations with open-source tools. Electrophoresis, 2021.
https://analyticalsciencejournals.onlinelibrary.wiley.com/doi/10.1002/elps.202000315

Solver and cases based on the work of Pablo A. Kler:

"Modeling And Simulation Of Microfluidic Chips For Analytical Applications" 
PhD. Thesis, Universidad Nacional del Litoral, Santa Fe, Argentina, 2010.

"SUPG and discontinuity-capturing methods for coupled fluid mechanics and
electrochemical transport problems" Computational Mechanics, February 2013,
Volume 51, Issue 2, pp 171–185.

Also presented at ENIEF 2016 and 2017

P.A Kler and S. Márquez Damián. Simulación numérica de procesos electroosmóticos
y electroforéticos mediante una plataforma modular basada en el Método de
Volúmenes Finitos. In Mecánica Computacional Vol XXXIV, 2016.

P.A Kler and S. Márquez Damián. Simulación numérica de procesos electroosmóticos
y electroforéticos mediante una plataforma modular basada en el Método de
Volúmenes Finitos. In Mecánica Computacional Vol XXXV, pages 1599–1599, 2017.

--------------------------------------------------------------------------------



