import pytest

from pathlib import Path

from foamlib import AsyncFoamCase


@pytest.fixture(scope="module")
async def serial_case():
    case = AsyncFoamCase(Path(__file__).parent / "serial")

    await case.clean()
    await case.run(parallel=False)

    return case

@pytest.fixture(scope="module")
async def parallel_case():
    case = AsyncFoamCase(Path(__file__).parent / "parallel")

    await case.clean()
    await case.run(parallel=True)

    return case

@pytest.mark.parametrize("name", ["ANILINE", "ACETIC_ACID", "CITRIC_ACID", "OXALIC_ACID", "HYDROCHLORIC_ACID"])
@pytest.mark.asyncio_cooperative
def test_analyte(serial_case, parallel_case, name):
    serial = serial_case[-1][f"ampholyte.{name}"].internal_field
    parallel = parallel_case[-1][f"ampholyte.{name}"].internal_field

    assert parallel == pytest.approx(serial, rel=1e-10)
