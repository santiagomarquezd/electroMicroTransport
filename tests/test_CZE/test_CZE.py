import pytest

from pathlib import Path

import numpy as np
from foamlib import AsyncFoamCase


@pytest.fixture(scope="module")
async def CZE_case():
    case = AsyncFoamCase(Path(__file__).parent / "CZE_test")

    await case.clean()
    await case.run()

    return case

@pytest.fixture(scope="module")
def preinitialized_CZE_case():
    return AsyncFoamCase(Path(__file__).parent / "CZE_test_initialized")


@pytest.mark.asyncio_cooperative
def test_initialization(CZE_case, preinitialized_CZE_case):
    for field in preinitialized_CZE_case[0]:
        if field.path.name.startswith("ampholyte."):
            assert CZE_case[0][field.path.name].internal_field == pytest.approx(field.internal_field)

@pytest.mark.asyncio_cooperative
@pytest.mark.parametrize("t", [0.1, 0.2])
def test_velocities(CZE_case, t):
    U = np.asarray(CZE_case[t]["U"].internal_field)
    assert np.linalg.norm(U[0]) == pytest.approx(np.linalg.norm(U[-1]), abs=1e-9, rel=0)
    assert np.linalg.norm(U, axis=-1) == pytest.approx(U[:,2], abs=1e-6)

@pytest.mark.asyncio_cooperative
def test_pyridine(CZE_case):
    concentrations = np.asarray([tdir["ampholyte.PYRIDINE"].internal_field for tdir in CZE_case])

    for c in concentrations:
        assert np.sum(c) == pytest.approx(np.sum(concentrations[0]), rel=1e-6, abs=0)

    for c1,c2 in zip(concentrations[:-1], concentrations[1:]):
        assert np.argmax(c1) <= np.argmax(c2)

@pytest.mark.asyncio_cooperative
def test_aniline(CZE_case):
    concentrations = np.asarray([tdir["ampholyte.ANILINE"].internal_field for tdir in CZE_case])
   
    for c1,c2 in zip(concentrations[:-1], concentrations[1:]):
        assert np.max(c1) > np.max(c2)
        assert np.argmax(c1) <= np.argmax(c2)
