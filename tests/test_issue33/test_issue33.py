import pytest

from pathlib import Path

from foamlib import AsyncFoamCase


@pytest.fixture(scope="module")
async def Rectangular_Current_Test_MPI_case():
    case = AsyncFoamCase(Path(__file__).parent / "Rectangular_Current_Test_MPI")

    await case.clean()
    await case.run()

    return case

@pytest.mark.asyncio_cooperative
def test_case_runs(Rectangular_Current_Test_MPI_case):
    pass
