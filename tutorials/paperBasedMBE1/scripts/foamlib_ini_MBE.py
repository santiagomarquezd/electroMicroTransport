#!/usr/bin/env python3

import numpy as np
import normaldistrib

from foamlib import FoamCase, FoamFile


xnod = np.array(FoamCase()[0].cell_centers().internal_field)[..., 2]
nnod = len(xnod)
len=(max(xnod)-min(xnod))*1000

with open("constant/electrolytes.txt") as arch:
    data=arch.readline().split()
    ndof=int(data[0])
    an_space=float(data[1])
    cat_space=float(data[2])
    amph=int(data[3])
    if amph:
        ph_data=arch.readline().split()
        n_amph=int(ph_data[1])
        ph_min=float(ph_data[2])
        ph_max=float(ph_data[3])
        amph_conc=float(ph_data[4])
    else:
        n_amph=0
    center=0.5*len*(1.0+an_space-cat_space)
    mid_l=(0.5)*len*(1-an_space-cat_space)

    for i in range(ndof):
        test=arch.readline().split()
        name=test[0]
        conc1=test[1]
        conc2=test[2]
        conc3=test[3]
        b1= conc1==conc2
        b2= conc2==conc3
        b3=not(b1 and b2)
        b4= float(conc1)<0

        if b3:
            internal_field = normaldistrib.erfv_stepr(1000*xnod,10,float(conc3),1.25,(1-cat_space)*len)+normaldistrib.erfv_stepl(1000*xnod,10,float(conc1),1.25,an_space*len)+normaldistrib.mserf(1000*xnod,10,float(conc2),1.25,center,mid_l)
        else:
            if b4:
                conc4=test[4]
                loc  =float(test[5])
                width=float(test[6])
                print(width)
                internal_field = normaldistrib.mserf(1000*xnod,40,float(conc4),0.,1000*loc,500*width)
                conc1=0.0
                conc3=0.0

            else:
                internal_field = float(conc1)

        with FoamCase()[0][f"ampholyte.{name}"] as out:
            out.dimensions = FoamFile.DimensionSet(length=-3, moles=1)
            out.boundary_field = {
                "fab": {"type": "empty"},
                "Wall": {"type": "empty"},
                "IN": {"type": "inletOutlet", "inletValue": float(conc1), "phi": "flux"},
                "OUT": {"type": "inletOutlet", "inletValue": float(conc3), "phi": "flux"},
            }
            out.internal_field = internal_field

    if amph:    
        for i in range(ndof,ndof+n_amph+1):
            internal_field = normaldistrib.mserf(1000*xnod[:,0],2,1.0*amph_conc/n_amph,1.25,center,mid_l)
            with FoamCase()[0][f"ampholyte.A{i-ndof}"] as out:
                out.dimensions = FoamFile.DimensionSet(length=-3, moles=1)
                out.boundary_field = {
                    "fab": {"type": "empty"},
                    "Wall": {"type": "empty"},
                    "IN": {"type": "inletOutlet", "inletValue": 0.0, "phi": "flux"},
                    "OUT": {"type": "inletOutlet", "inletValue": 0.0, "phi": "flux"},
                }
                out.internal_field = internal_field
