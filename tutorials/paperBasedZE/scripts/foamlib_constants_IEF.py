#!/usr/bin/env python3

import numpy as np

from foamlib import FoamFile
from electrolytes import database

with open("constant/electrolytes.txt") as arch:
    data=arch.readline().split()
    ndof=int(data[0])
    an_space=float(data[1])
    cat_space=float(data[2])
    amph=int(data[3])
    nreact=int(data[4])
    Kn=np.zeros((ndof+1,6))
    diffn=np.zeros(ndof+1)
    movn=np.zeros((ndof+1,6))
    
    ampholytes = {}
    if amph:
        ph_data=arch.readline().split()
        n_amph=int(ph_data[1])
        ph_min=float(ph_data[2])
        ph_max=float(ph_data[3])
        ph_range=ph_max-ph_min
        mA=1.0*ph_range*np.asarray(range(n_amph+1))/n_amph + ph_min
        mD=(3-abs(2.78*np.asarray(range(n_amph+1))/n_amph-1.2))/2.0
        pkam1=mA+mD
        pkam2=mA+2.5*mD
        pkap1=mA-mD
        pkap2=mA-2.5*mD

        mov_amph=1e-8*(2+abs(1.40*np.asarray(range(n_amph+1))/n_amph-0.8))
        diff_amph=mov_amph*0.02585065036
        Kn=np.zeros((ndof+n_amph+1,6))
        diffn=np.zeros(ndof+n_amph+1)
        movn=np.zeros((ndof+n_amph+1,6))
        for i in range(ndof,ndof+n_amph+1):
            Kn[i]=10**(3-np.asarray([-3,pkap2[i-ndof],pkap1[i-ndof],pkam1[i-ndof],pkam2[i-ndof],15]))
            # Kn[i]=10**(3-np.asarray([-3,-2,pkap1[i-ndof],pkam1[i-ndof],15,16]))
            movn[i]=mov_amph[i-ndof]
            diffn[i]=diff_amph[i-ndof]
            ampholytes[f"A{i-ndof}"] = {
                "diffusivity": diffn[i],
                "mobility": movn[i],
                "pk": Kn[i]
            }
    
    n_dis=6
    dofnames=[]
    for i in range(ndof):
        subs=arch.readline().split()[0]
        dofnames.append(subs)
        
    K2n=np.zeros((ndof,ndof,ndof))
    K1n=np.zeros((ndof,ndof))
    for i in range(nreact):
        data=arch.readline().split()
        kb=0.5*float(data[1])
        ku=float(data[2])
        r1=int(data[3])
        r2=int(data[4])
        prod=int(data[5])
        l1=[(prod,r1,r2),(prod,r2,r1)]
        l2=[(r1,r1,r2),(r1,r2,r1),(r2,r2,r1),(r2,r1,r2)]#,(prod,r1,prod),(prod,prod,r1),(prod,r2,prod),(prod,prod,r2)]
        l3=[(r1,prod),(r2,prod)]
        l4=(prod,prod)
        for j in l1:
            K2n[j]=-kb
        for j in l2:
            K2n[j]=kb
        for j in l3:
            K1n[j]=-ku
        K1n[l4]=+ku
    i=0    
    for name in dofnames:
        props=database[name]
        diffn[i]=props.diffusivity()
        movn[i]=np.asarray(props.mobilities())
        Kn[i]=10**(3-np.asarray(props.pkas()))
        ampholytes[name] = {
            "diffusivity": diffn[i],
            "mobility": movn[i],
            "pk": Kn[i],
            "K1": K1n[i].flatten(),
            "K2": K2n[i].flatten()
        }
        print(name)
        i+=1

with FoamFile("constant/ampholyteProperties") as out:
    out["ampholytes"] = list(ampholytes.items())
    out["sigma_0"] = FoamFile.Dimensioned(2e-4, FoamFile.DimensionSet(mass=-1, length=-3, time=3, current=2), "sigma_0")
    out["phip"] = FoamFile.Dimensioned(0.7, FoamFile.DimensionSet(), "phip")
    out["taup"] = FoamFile.Dimensioned(2.9, FoamFile.DimensionSet(), "taup")
    out["sc"] = FoamFile.Dimensioned(5e-5, FoamFile.DimensionSet(length=1), "sc")
    out["se"] = FoamFile.Dimensioned(5e-5, FoamFile.DimensionSet(length=1), "se")
    out["Kperm"] = FoamFile.Dimensioned(7.8e-13, FoamFile.DimensionSet(length=2), "Kperm")
    out["zeta"] = FoamFile.Dimensioned(-0.015, FoamFile.DimensionSet(charge=1, length=2, mass=-3, current=-1), "zeta")
    out["rho_fluid"] = FoamFile.Dimensioned(1000.0, FoamFile.DimensionSet(mass=1, length=-3), "rho_fluid")
