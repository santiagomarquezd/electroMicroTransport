-------------------------------------------------------------------------------
      electroMicroTransport | Copyright (C) 2016-2020 Santiago Marquez Damian
                            |                         Pablo Alejandro Kler 
-------------------------------------------------------------------------------

This tutorial example performs a paper-based zone electrophoresis experiment
with a 500 μM sodium sample which is placed in a 60 mm long, 1 mm width Whatman
#1 channel. The injection channel and detector positions are 13 and 50 mm,
respectively. BGE is composed of 20 mM MES and 20 mM Histidine (pH 6.1) and a
2.5 kV electric potential is applied across the channel. In order to change the
electrolyte composition edit the file ./constant/electrolytes.txt.

Regarding the grid, the capillary tube was divided into 12000 linear segments.
The simulation runs for 200 seconds giving the concentration profiles for
sodium showing the characteristic shape expected due to electrodispersion.

-------------------------------------------------------------------------------
Bug reporting and testing by Federico Schaumburg and Gabriel Gerlero
Tested with OpenFOAM(R) v.1912+

