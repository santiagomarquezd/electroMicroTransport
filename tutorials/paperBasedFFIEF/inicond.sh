#!/bin/bash
rm -rf processor*
foamListTimes -rm
rm ./0/ampholyte*
rm ./constant/ampholyteProperties
mv 0/Phi 0/org.Phi
mv 0/org.U 0/U
blockMesh
postProcess -func writeCellCentres

scripts/foamlib_constants_IEF.py

python3 ./scripts/Foam_ini_IEF_new.py -i ./constant/electrolytes_ief.txt -p ./0/Cx

potentialFoam

mv 0/org.Phi 0/Phi
cp 0/U.slip 0/org.U
