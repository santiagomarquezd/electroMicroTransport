-------------------------------------------------------------------------------
      electroMicroTransport | Copyright (C) 2016-2018 Santiago Marquez Damian
                            |                         Pablo Alejandro Kler
			    
-------------------------------------------------------------------------------

This example solves the isotachophoretic separation of strong cations Lithium,
Sodium and Potassium, where the counterion is Acetate. The concentrations of
these electrolytes can be modified in the ./constant/electrolytes.txt file. A
constant electric current of 2000 A/m^2 is applied on a 1 cm capillary column
discretized with 4000 linear segments.


-------------------------------------------------------------------------------

Bug reporting and testing by Federico Schaumburg
Tested with OpenFOAM(R) v.1712+

