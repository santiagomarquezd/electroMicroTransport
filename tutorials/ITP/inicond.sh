#!/bin/bash
rm -rf processor*
foamListTimes -rm
rm ./0/ampholyte*
rm ./constant/ampholyteProperties
blockMesh
postProcess -func writeCellCentres

scripts/foamlib_constants_IEF.py

python3 ./scripts/Foam_ini_IEF.py -i ./constant/electrolytes.txt -p ./0/Cz

