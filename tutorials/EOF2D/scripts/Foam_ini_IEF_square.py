import numpy as np
import sys, getopt
import normaldistrib

def make_ini(argv):
    inputfile = ''
    pointsfile  = ''
    try:
        opts, args = getopt.getopt(argv,"hi:p:",["ifile="])
    except getopt.GetoptError:
        print('Foam_constants.py -i <inputfile> ')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-i", "--ifile"):
            inputfile = arg
        
    print(inputfile)                 

    
    
       
    arch=open(inputfile,'r')
    data=arch.readline().split()
    ndof=int(data[0])
    an_space=float(data[1])
    cat_space=float(data[2])
    amph=int(data[3])
    n_amph=0
    

   
    for i in range(ndof):
        test=arch.readline().split()
        name=test[0]
        conc1=test[1]
        conc2=test[2]
        conc3=test[3]
        b1= conc1==conc2
        b2= conc2==conc3
        b3=not(b1 and b2)
        if 0:
            solte[:,i]=normaldistrib.erfv_stepr(1000*xnod[:,0],2,float(conc3),1.25,(1-cat_space)*len)+normaldistrib.erfv_stepl(1000*xnod[:,0],2,float(conc1),1.25,an_space*len)+normaldistrib.mserf(1000*xnod[:,0],2,float(conc2),1.25,center,mid_l)
            dofaux= 'nonuniform List<scalar>\n'+str(nnod)+'\n (\n'
            dofs=' '.join(str(i)+'\n' for i in solte[:,i])
        else:
            dofs=''
            dofaux= 'uniform '+ conc2+';'
        outputfile='./0.orig/ampholyte.'+name

        aux=open(outputfile,'w')
        aux.writelines(header('ampholyte.'+name))
        aux.writelines(dofaux)
        aux.writelines(dofs)
    #    bounds=0##############OJOOOOOOOOOOOOOOOOOO
        aux.writelines(footer(conc1,conc3)) 
        
        aux.close()
    if amph:    
        for i in range(ndof,ndof+n_amph+1):
            solte[:,i]=distrib.mserf(1000*xnod[:,0],2,1.0*amph_conc/n_amph,1.25,center,mid_l)
            outputfile='./0/ampholyte.'+'A'+str(i-ndof)
            dofaux= 'nonuniform List<scalar>\n'+str(nnod)+'\n (\n'
            dofs=' '.join(str(i)+'\n' for i in solte[:,i])
            aux=open(outputfile,'w')
            aux.writelines(header('ampholyte.A'+str(i-ndof),nnod))
            aux.writelines(dofaux)
            aux.writelines(dofs)
            aux.writelines(footer(0.0,0.0))
            aux.close()
    
    return 



def footer(conc1,conc2):
    footer='\n\nboundaryField\n{\n    wallV\n    {\n        type            zeroGradient;\n    }\n    wallH\n    {\n        type            zeroGradient;\n    }\n    axis\n    {\n        type            empty;\n    }\n    inletO\n    {\n        type            inletOutlet;\n inletValue           uniform '+str(conc2)+';\n    phi             flux;\n   }\ninletS\n    {\n        type            inletOutlet;\n inletValue           uniform '+str(conc2)+';\n    phi             flux;\n   }\ninletN\n    {\n        type            inletOutlet;\n inletValue           uniform '+str(conc1)+';\n    phi             flux;\n   }\ninletE\n    {\n        type            inletOutlet;\n inletValue           uniform '+str(conc2)+';\n    phi             flux;\n   }\n \n}\n'

    return footer


def header(name):
    header1='/*--------------------------------*- C++ -*----------------------------------*\ \n| =========                 |                                                 |\n| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |\n|  \\    /   O peration     | Version:  v3.0+                                 |\n|   \\  /    A nd           | Web:      www.OpenFOAM.com                      |\n|    \\/     M anipulation  |                                                 |\n\*---------------------------------------------------------------------------*/\n FoamFile{\n    version     2.0;\n    format      ascii;\n    class       volScalarField;\n    location    "0";\n    object      '+name+';\n}\n// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //\n\n dimensions      [0 -3 0 0 1 0 0];\n\n internalField  '
    return header1


if __name__ == "__main__":
   make_ini(sys.argv[1:])
