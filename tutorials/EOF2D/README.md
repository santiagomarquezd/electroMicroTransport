-------------------------------------------------------------------------------
      electroMicroTransport | Copyright (C) 2016-2020 Santiago Marquez Damian
                            |                         Pablo Alejandro Kler 
-------------------------------------------------------------------------------

This tutorial example performs a CZE separation of aminoacids alanine,
glycine and phenylalanine by using a background buffer of Acetic acid
2.3 M. A double L microchip is used with three auxiliary columns of 1
cm and one separation column of 4 cm. Channel width is 100 microns.
In order to change the electrolyte composition edit the file
./constant/aminoacids.txt.

The analysis consist of two stages: an injection and a separation. In
order to run the first one, execute the script ./Allrun.pre, and then
electroMicroTransport until time reachs 60 seconds.  Then, in order to
run the separation, execute the script ./Allrun-sep.pre, and then, run
again electroMicroTransport.

-------------------------------------------------------------------------------
Bug reporting and testing by Federico Schaumburg and Gabriel Gerlero
Tested with OpenFOAM(R) v.1712+, 18.12+, and 19.12+

