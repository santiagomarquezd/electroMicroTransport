/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  plus                                  |
|   \\  /    A nd           | Web:      www.OpenFOAM.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       volScalarField;
    location    "0";
    object      Phi;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

dimensions      [1 2 -3 0 0 -1 0];


internalField   uniform 0; 

boundaryField
{
    inletO
    {
        type            zeroGradient;
    }
    inletS
    {
        type            fixedValue;
        value           uniform 0;
    }
    inletN
    {
        type            fixedValue;
        value           uniform 250;
    }
    inletE
    {
        type            zeroGradient;
    }
    wallV
    {
        type            zeroGradient;
    }
    wallH
    {
        type            zeroGradient;
    }
    axis
    {
        type            empty;
    }
}


// ************************************************************************* //
