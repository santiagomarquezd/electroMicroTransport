-------------------------------------------------------------------------------
      electroMicroTransport | Copyright (C) 2016-2021 Santiago Marquez Damian
                            |                         Pablo Alejandro Kler 
-------------------------------------------------------------------------------

This tutorial example performs a paper-based zone electrophoresis experiment
with a XXXX sample which is placed in a XXX mm long, XX mm width Whatman
#1 channel. The injection channel and detector positions are XXX and XXXX mm,
respectively. BGE is composed of XXXX mM XXXX and XXXX mM XXXXX (pH 6.1) and a
XXXXX kV electric potential is applied across the channel. In order to change the
electrolyte composition edit the file ./constant/electrolytes.txt.

Regarding the grid, the capillary tube was divided into XXXXX linear segments.
The simulation runs for XXXX seconds giving the concentration profiles for
samples showing the characteristic shape expected due to electrodispersion.

-------------------------------------------------------------------------------
Bug reporting and testing by Federico Schaumburg and Gabriel Gerlero
Tested with OpenFOAM(R) v.1912+

