-------------------------------------------------------------------------------
      electroMicroTransport | Copyright (C) 2016-2018 Santiago Marquez Damian
                            |                         Pablo Alejandro Kler 
-------------------------------------------------------------------------------


In this case a IEF problem is solved on a capillary tube of 5 cm, where 35
ideal carrier ampholytes are initially uniformly distributed with a
concentration of 1 mM each. In order to modify this, edit the
./constant/ampholytes.txt file. The carrier ampholytes have pI values ranging
uniformly from 3.1 to 9.9. A constant electric field of 300 V/cm is applied to
the boundaries, which are closed to any kind of mass flow. The grid used for
simulation consisted of 10000 linear segments. 



-------------------------------------------------------------------------------

Bug reporting and testing by Federico Schaumburg
Tested with OpenFOAM(R) v.1712+

