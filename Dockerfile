FROM microfluidica/openfoam:2406

ARG ELECTROMICROTRANSPORT_DIR=/usr/local/src/electroMicroTransport

ENV ELECTROMICROTRANSPORT_APP=${ELECTROMICROTRANSPORT_DIR}/applications
ENV ELECTROMICROTRANSPORT_SRC=${ELECTROMICROTRANSPORT_DIR}/libraries
ENV ELECTROMICROTRANSPORT_TUTORIALS=${ELECTROMICROTRANSPORT_DIR}/tutorials
ENV ELECTROMICROTRANSPORT_TESTS=${ELECTROMICROTRANSPORT_DIR}/tests
ARG ELECTROMICROTRANSPORT_COPYING=${ELECTROMICROTRANSPORT_DIR}/COPYING

# Install tutorial dependencies
COPY tutorials/requirements.txt ${ELECTROMICROTRANSPORT_TUTORIALS}/

ARG VIRTUAL_ENV=/opt/venv

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends \
    python3-venv \
 && rm -rf /var/lib/apt/lists/* \
 && python3 -m venv ${VIRTUAL_ENV} \
 && source ${VIRTUAL_ENV}/bin/activate \
 && pip install --no-cache-dir -r ${ELECTROMICROTRANSPORT_TUTORIALS}/requirements.txt \
# set up electrolytes bash completion for all users
 && echo "" >> /etc/bash.bashrc \
 && echo "# electrolytes bash completion" >> /etc/bash.bashrc \
 && electrolytes --show-completion bash >> /etc/bash.bashrc

ENV PATH="${VIRTUAL_ENV}/bin:$PATH"

# smoke test
RUN electrolytes --help

# Copy the source code, compile and install the toolbox for all users
COPY applications ${ELECTROMICROTRANSPORT_APP}
COPY libraries ${ELECTROMICROTRANSPORT_SRC}
COPY Allwmake Allwclean ${ELECTROMICROTRANSPORT_DIR}/

RUN ${ELECTROMICROTRANSPORT_DIR}/Allwmake -j -prefix=group \
 && ${ELECTROMICROTRANSPORT_DIR}/Allwclean \
# smoke test
 && electroMicroTransport -help

# Include tutorials
COPY tutorials ${ELECTROMICROTRANSPORT_TUTORIALS}

# Include tests
COPY tests ${ELECTROMICROTRANSPORT_TESTS}

# Copy the license
COPY COPYING ${ELECTROMICROTRANSPORT_COPYING}
