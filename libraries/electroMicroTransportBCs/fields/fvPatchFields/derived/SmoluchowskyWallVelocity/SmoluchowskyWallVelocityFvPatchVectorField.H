/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2016 OpenCFD Ltd.
     \\/     M anipulation  |
-------------------------------------------------------------------------------
      electroMicroTransport | Copyright (C) 2016-2018 Santiago Marquez Damian
                            |                         Pablo Alejandro Kler 
-------------------------------------------------------------------------------
License
    This file is part of electroMicroTransport, which is an unofficial
    extension to OpenFOAM.
    
    electroMicroTransport is free software: you can redistribute it and/or
    modify it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    IsoAdvector is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.
        
    You should have received a copy of the GNU General Public License
    along with electroMicroTransport. If not, see
    <http://www.gnu.org/licenses/>.

Class
    Foam::SmoluchowskyWallVelocityFvPatchVectorField


Description
    This boundary condition provides an electric double layer boundary
    condition for velocity

    \heading Patch usage

    Example of the boundary condition specification:
    \verbatim
    myPatch
    {
        type            SmoluchowskyWallVelocity;
        value           uniform (0 0 0); // initial value
    }
    \endverbatim


SourceFiles
    SmoluchowskyWallVelocityFvPatchVectorField.C

\*---------------------------------------------------------------------------*/

#ifndef SmoluchowskyWallVelocityFvPatchVectorField_H
#define SmoluchowskyWallVelocityFvPatchVectorField_H

#include "fvPatchFields.H"
#include "fixedValueFvPatchFields.H"

#include "electromagneticConstants.H"
#include "physicoChemicalConstants.H"

#include "ampholyteMixture.H"
//#include "stdio.h"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
             Class SmoluchowskyWallVelocityFvPatchVectorField Declaration
\*---------------------------------------------------------------------------*/

class SmoluchowskyWallVelocityFvPatchVectorField
:
    public fixedValueFvPatchVectorField
{
    // Private data

       //- Name of electric field
       word EName_;
       
       // - dynamic viscosity
       static const scalar mu_;
       
       // - Reference to hydron field
       //const volScalarField& HPlus_;
       
       // - Reference to ampholytes
       //const ampholyteMixture& ampholytes_;

       static const scalar T_;
       
       // - To store zita field
       scalarField zita_;

       bool  solve_zita_;

       scalar zeta_value_;
  
       // -> This parameter should be read from dictionaries
       static const scalar epsilonr_;
       
       // -> This parameter should be read from dictionaries
       static const scalar pKs_;
       
       // -> This parameter should be read from dictionaries
       static const scalar z_;
       
       // -> This parameter should be read from dictionaries
       static const scalar zitaNTol_;
       
       // -> This parameter should be read from dictionaries
       static const label zitaNIter_;


public:

    //- Runtime type information
    TypeName("SmoluchowskyWallVelocity");


    // Constructors

        //- Construct from patch and internal field
        SmoluchowskyWallVelocityFvPatchVectorField
        (
            const fvPatch&,
            const DimensionedField<vector, volMesh>&
        );

        //- Construct from patch, internal field and dictionary
        SmoluchowskyWallVelocityFvPatchVectorField
        (
            const fvPatch&,
            const DimensionedField<vector, volMesh>&,
            const dictionary&
        );

        //- Construct by mapping given
        //  SmoluchowskyWallVelocityFvPatchVectorField
        //  onto a new patch
        SmoluchowskyWallVelocityFvPatchVectorField
        (
            const SmoluchowskyWallVelocityFvPatchVectorField&,
            const fvPatch&,
            const DimensionedField<vector, volMesh>&,
            const fvPatchFieldMapper&
        );

        //- Construct as copy
        SmoluchowskyWallVelocityFvPatchVectorField
        (
            const SmoluchowskyWallVelocityFvPatchVectorField&
        );

        //- Construct and return a clone
        virtual tmp<fvPatchVectorField> clone() const
        {
            return tmp<fvPatchVectorField>
            (
                new SmoluchowskyWallVelocityFvPatchVectorField(*this)
            );
        }

        //- Construct as copy setting internal field reference
        SmoluchowskyWallVelocityFvPatchVectorField
        (
            const SmoluchowskyWallVelocityFvPatchVectorField&,
            const DimensionedField<vector, volMesh>&
        );

        //- Construct and return a clone setting internal field reference
        virtual tmp<fvPatchVectorField> clone
        (
            const DimensionedField<vector, volMesh>& iF
        ) const
        {
            return tmp<fvPatchVectorField>
            (
                new SmoluchowskyWallVelocityFvPatchVectorField(*this, iF)
            );
        }


    // Member functions
    
        scalarField coso();
        
        scalarField cosoPrime();
    
        //- Zita correction
        void zitaCorrect();

        //- Update the coefficients associated with the patch field
        virtual void updateCoeffs();

        //- Write
        virtual void write(Ostream&) const;
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#include "volFields.H"
#include "SmoluchowskyWallVelocityFvPatchVectorField.H"

namespace Foam
{
    template<class SmoluchowskyBC>
    inline void setZitaVals
    (
       volVectorField::Boundary& bf
    )
    {
        forAll(bf, patchi)
        {
            if (isA<SmoluchowskyBC>(bf[patchi]))
            {
                refCast<SmoluchowskyBC>(bf[patchi]).zitaCorrect();
            }
        }
    }
  
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
